﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TouchPortalAPI.Events;
using TouchPortalAPI.Helpers;

namespace TouchPortalAPI
{
    namespace Helpers
    {
        public class Methods
        {
            public static bool PublicInstancePropertiesEqual<T>(T self, T to, params string[] ignore) where T : class
            {
                if (self != null && to != null)
                {
                    Type type = typeof(T);
                    List<string> ignoreList = new List<string>(ignore);
                    foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                    {
                        if (!ignoreList.Contains(pi.Name))
                        {
                            object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                            object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                            if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                            {
                                return false;
                            }
                        }
                    }
                    return true;
                }
                return self == to;
            }
        }
        public class TouchPortalSocketHelper
        {
            private readonly Socket mainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            public TouchPortalSocketHelper()
            {
                Connected += onConnect;
            }

            private void onConnect(object sender, EventArgs args) => ProcessLinesAsync(mainSocket);
            Task ProcessLinesAsync(Socket socket)
            {
                var pipe = new Pipe();
                Task writing = FillPipeAsync(socket, pipe.Writer);
                Task reading = ReadPipeAsync(pipe.Reader);

                return Task.WhenAll(reading, writing);
            }

            async Task FillPipeAsync(Socket socket, PipeWriter writer)
            {
                const int minimumBufferSize = 1024 * 1024;

                while (true)
                {
                    // Allocate at least 512 bytes from the PipeWriter
                    Memory<byte> memory = writer.GetMemory(minimumBufferSize);
                    try
                    {
                        //Turn memory space into ArraySegment for socket use
                        if (!MemoryMarshal.TryGetArray((ReadOnlyMemory<byte>)memory, out ArraySegment<byte> arraySegment))
                        {
                            throw new InvalidOperationException("Buffer backed by array was expected");
                        }

                        int bytesRead = await SocketTaskExtensions.ReceiveAsync(socket, arraySegment, SocketFlags.None);
                        if (bytesRead == 0)
                        {
                            break;
                        }
                        // Tell the PipeWriter how much was read from the Socket
                        writer.Advance(bytesRead);
                    }
                    catch (Exception ex)
                    {
                        //LogError(ex);
                        break;
                    }

                    // Make the data available to the PipeReader
                    FlushResult result = await writer.FlushAsync();

                    if (result.IsCompleted)
                    {
                        break;
                    }
                }

                // Tell the PipeReader that there's no more data coming
                writer.Complete();
            }

            void ProcessLine(ReadOnlySequence<byte> bytes)
            {
                var str = System.Text.Encoding.Default.GetString(bytes.ToArray());
                MessageReceivedArgs args = new MessageReceivedArgs();
                args.Message = str;
                OnMessageRecevied(args);
            }
            async Task ReadPipeAsync(PipeReader reader)
            {
                while (true)
                {

                    ReadResult result = new ReadResult();
                    try
                    {
                        result = await reader.ReadAsync();
                    }catch(Exception e) {
                        Console.WriteLine(e.Message);
                    };

                    ReadOnlySequence<byte> buffer = result.Buffer;
                    SequencePosition? position = null;

                    do
                    {
                        // Look for a EOL in the buffer
                        position = buffer.PositionOf((byte)'\n');

                        if (position != null)
                        {
                            // Process the line
                            ProcessLine(buffer.Slice(0, position.Value));

                            // Skip the line + the \n character (basically position)
                            buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
                        }
                    }
                    while (position != null);

                    // Tell the PipeReader how much of the buffer we have consumed
                    reader.AdvanceTo(buffer.Start, buffer.End);

                    // Stop reading if there's no more data coming
                    if (result.IsCompleted)
                    {
                        break;
                    }
                }

                // Mark the PipeReader as complete
                reader.Complete();
            }

            public bool connect(IPEndPoint endpoint)
            {
                mainSocket.Connect(endpoint);
                EventArgs args = new EventArgs();
                OnConnected(args);
                return mainSocket.Connected;
            }

            public void Send(string str)
            {
                Console.WriteLine("Sending TP : " + str);
                mainSocket.Send(System.Text.Encoding.ASCII.GetBytes(String.Format("{0}{1}", str, "\n")));
            }
            public void Send(object obj) => Send(JsonConvert.SerializeObject(obj));

            protected virtual void OnConnected(EventArgs e)
            {
                Connected?.Invoke(this, e);
            }

            public event EventHandler<EventArgs> Connected;
            protected virtual void OnMessageRecevied(MessageReceivedArgs e, MessageFormat format = MessageFormat.String) => MessageReceived?.Invoke(this, e);
            public enum MessageFormat
            {
                JSON,
                String
            }

            public event EventHandler<MessageReceivedArgs> MessageReceived;
            public class MessageReceivedArgs : EventArgs
            {
                public MessageReceivedArgs()
                {
                    timestamp = new DateTime();
                }
                public DateTime timestamp { get; set; }
                public string Message { get; set; }
            }
        }
    }

    namespace DataTypes
    {
        public class ChoiceUpdateData
        {
            private bool _specific = false;
            public string type { get => "choiceUpdate"; }
            public string id { get; set; }
            public string[] value { get; set; }
            public string instanceId { get; set; }
            public bool ShouldSerializeinstanceId() => !String.IsNullOrEmpty(instanceId);
        }
        public class ActionData
        {
            public string id { get; }
            public string value { get; }
        }
        public class State
        {
            public string type { get => "stateUpdate"; }
            public string id;
            public string value;
        }
        public class Pair
        {
            public string type { get => "pair"; }
            public string id;
        }
    }

    namespace Events
    {
        public class ClosingArgs
        {
            public string type { get => "closePlugin"; }
            public string pluginId { get; set; }
        }
        public class ListChangedArgs
        {
            public string type { get; }
            public string pluginId { get; set; }
            public string actionId { get; }
            public string listId { get; }
            public string instanceId { get; }
            public string value { get; }
        }
        public class PairArgs : EventArgs
        {
            public string type { get => "info"; }
            public string sdkVersion { get; set; }
            public string tpVersionString { get; set; }
            public string tpVersionCode { get; set; }
            public string pluginVersion { get; set; }
        }
        public class ActionTriggeredArgs : EventArgs
        {
            //[JsonIgnore]
            //public List<ActionData> dataList = new List<ActionData>();
            public string type { get => "action"; }
            public string pluginId { get => pluginId; }
            public string actionId { get; }
            public DataTypes.ActionData[] data { get; }
        }
        public class ConnectedArgs
        {
            public DateTime timestamp { get; }
        }
        public class MessageReceivedArgs : Helpers.TouchPortalSocketHelper.MessageReceivedArgs
        {

            public string removeReturn()
            {
                string output = Message.Replace("}\r", "}");
                return output;
            }
            public dynamic toObject()
            {
                var obj = JObject.Parse(removeReturn());
                return obj;
            }
        }

    }
    public class TPApi
    {
        #region Properties
        private TouchPortalSocketHelper socketHelper;
        public string pluginID { get; set; }
        public bool paired { get; private set; }
        public JObject config { get; private set; }
        #endregion

        public TPApi()
        {
            configUpdate();
            socketHelper = new TouchPortalSocketHelper();
            socketHelper.Connected += onConnected;
            socketHelper.MessageReceived += onMessageReceived;
            Paired += onPaired;
        }

        #region Helpers
        private T Downcast<T>(object obj)
        {
            var serializedParent = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(serializedParent);
        }
        private static IEnumerable<Type> GetClasses(string nameSpace)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            return asm.GetTypes()
                .Where(type => type.Namespace == nameSpace && type.IsClass);
        }
        #endregion

        #region Public Methods
        public bool connect(IPEndPoint endpoint)
        {
            if (String.IsNullOrEmpty(pluginID)) throw new Exception("You must providate a plugin id.");
            return socketHelper.connect(endpoint);
        }
        public bool connect(string ip, string port)
        {
            IPAddress tpIP = IPAddress.Parse(ip);
            IPEndPoint tpEndPoint = new IPEndPoint(tpIP, 12136);
            return connect(tpEndPoint);
        }
        public bool connect() => connect("127.0.0.1", "12136");
        public void pair(string id) => socketHelper.Send(new DataTypes.Pair { id = id });

        public JObject configUpdate()
        {
            
            string configPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "TouchPortal", "config.properties");
            char[] delimiters = new char[] { '\r', '\n' };
            string configText = System.IO.File.ReadAllText(configPath)
                .Split(delimiters).Where(row => row.IndexOf('=') > -1).Select((row) =>
                {
                    string[] split = row.Split('=');
                    return String.Format("\"{0}\":\"{1}\"", split[0], split[1].Replace("\\", ""));
                }).Aggregate((acc, x) => String.Format("{0},{1}", acc, x));
            config = JObject.Parse(String.Format("{{{0}}}", configText));
            return config;
        }
        public void stateUpdate(DataTypes.State state) => socketHelper.Send(state);
        public void choiceUpdate(DataTypes.ChoiceUpdateData data) => socketHelper.Send(data);
        #endregion

        #region OnEvents
        protected virtual void onPaired(object sender, Events.PairArgs args)
        {
            paired = true;
        }
        protected virtual void onMessageReceived(object sender, Helpers.TouchPortalSocketHelper.MessageReceivedArgs e)
        {
            MessageReceivedArgs args = Downcast<MessageReceivedArgs>(e);


            // If the message has a type, let's find the associated Type and OnEvent
            if (args.toObject().type != null)
            {
                object obj = null;
                try
                {
                    obj = GetClasses("TouchPortalAPI.Events")
                        .Select(c => Activator.CreateInstance(c))
                        .First(c => ((dynamic)JObject.Parse(JsonConvert.SerializeObject(c))).type == ((dynamic)JObject.Parse(args.removeReturn())).type);

                }
                catch (Exception err) { 
                    Console.WriteLine(err.Message); 
                }

                var method = typeof(TPApi).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
                    .Where(meth => meth.Name.StartsWith("on"))
                    .First(meth =>
                    {
                        IEnumerable<ParameterInfo> @params = meth.GetParameters();
                        return @params.ToArray()[0].ParameterType == obj.GetType();
                    });

                var data = JsonConvert.DeserializeObject(args.removeReturn(), obj.GetType());
                try
                {
                    method.Invoke(this, new[] { data, null });
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
            }

            MessageReceived?.Invoke(this, args);
        }
        protected virtual void onConnected(object sender, EventArgs args)
        {
            pair(pluginID);
        }
        protected virtual void onPaired(Events.PairArgs e, MessageFormat format = MessageFormat.JSON)
        {
            Paired?.Invoke(this, e);
        }
        protected virtual void onMessageReceived(Events.MessageReceivedArgs e, MessageFormat format = MessageFormat.JSON)
        {
            MessageReceived?.Invoke(this, e);
        }
        #endregion

        #region Event Handlers
        public event EventHandler<Events.MessageReceivedArgs> MessageReceived;
        public event EventHandler<Events.PairArgs> Paired;
        public event EventHandler<Events.ConnectedArgs> Connected;
        public event EventHandler<Events.ActionTriggeredArgs> ActionTriggered;
        public event EventHandler<Events.ListChangedArgs> ListChanged;
        public event EventHandler<Events.ListChangedArgs> Closing;
        #endregion

        #region Enums
        public enum MessageFormat
        {
            JSON,
            String
        }
        #endregion
    }
}

